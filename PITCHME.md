@title[Strona tytułowa]
# Predykcja notowań akcji spółek indeksu WIG20 na podstawie informacji prasowych

@snap[south-west]
Marek Jędryka
@snapend

@snap[south-east]
dr inż. Teresa Mroczek
@snapend

---

## Agenda

@ol
- Pozyskanie danych
- Wykrycie trendów w notowaniach akcji
- Analiza tekstów
- Prognoza kursu akcji
- Wyniki
@olend

---

## Pozyskanie danych

@snap[center]
@fa[file-download step]
@fa[arrow-down transition]
@fa[cog step]
@fa[arrow-down transition]
@fa[file-csv step]
@snapend

@snap[west]
![IMAGE](assets/img/logo_GPW.png)
@snapend

@snap[east]
![IMAGE](assets/img/bankier-logo.png)
@snapend

----?image=assets/img/punkty-1.png&size=contain

---

## Wyniki

Skuteczność ogółem: 52% względna, 24% bezwzględna.

<canvas data-chart="bar">
<!--
{
 "data": {
  "labels": ["paliwa i energia"," finanse"," dobra konsumpcyjne"," technologie"],
  "datasets": [
   {
    "data":[37.5,73.3,50.0,20.0],
    "label":"Skuteczność względna",
    "backgroundColor":"rgba(20,20,220,.8)"
   },
   {
    "data":[17.7, 30.6, 32.0, 6.3],
    "label":"Skuteczność bezwzględna",
    "backgroundColor":"rgba(120,120,220,.8)"
   }
  ]
 },
 "options": { "responsive": "true" }
}
-->
</canvas>

3 spółki odrzucono.

---

### Dziękuję za uwagę
